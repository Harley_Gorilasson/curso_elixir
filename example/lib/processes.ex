defmodule Processes do
  def greet do
    receive do
    {:ok, msg} ->
      IO.puts("OK --> #{msg}")
      greet()

    {:error, msg} ->
      IO.puts("ERROR --> #{msg}")
      greet()
    end
  end
end
