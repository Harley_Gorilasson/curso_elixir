defmodule Lists do
  def len(list), do: list_len(list, 0)

  defp list_len([], n), do: n
  defp list_len([ _ | tail], n), do: list_len(tail, n + 1)
end
