defmodule Enums do
  def map(list, function) when is_list(list) and is_function(function) do
    map(list, [], function)
  end

  defp map([], result, _), do: result
  defp map([h | t], result, function) do
    map(t, result ++ [function.(h)], function)
  end

  def filter(list, function) when is_list(list) and is_function(function) do
      filter(list, [], function)
  end

  defp filter([], result, _), do: result
  defp filter([h | t], result, function) do
    cond do
      function.(h) == true  -> filter(t, result ++ [h], function)
      function.(h) == false -> filter(t, result, function)
      true                  -> filter(t, result, function)
    end
  end
end
