defmodule FizzBuzz do
  def run() do
    Ranges.new(1,100)
    |> fizzbuzz
  end

  def fizzbuzz(list) do
    fizzbuzz(list, [])
  end

  defp fizzbuzz([], result), do: result

  defp fizzbuzz([h | t], result) when rem(h, 15) == 0 do
    fizzbuzz(t, result ++ [:fizzbuzz])
  end
  defp fizzbuzz([h | t], result) when rem(h, 3) == 0 do
    fizzbuzz(t, result ++ [:fizz])
  end
  defp fizzbuzz([h | t], result) when rem(h, 5) == 0 do
    fizzbuzz(t, result ++ [:buzz])
  end
  defp fizzbuzz([h | t], result) do
    fizzbuzz(t, result ++ [h])
  end
end
