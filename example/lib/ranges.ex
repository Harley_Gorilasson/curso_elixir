defmodule Ranges do
  def new(from, to) when is_integer(from) and is_integer(to) do
    cond do
      from < to   -> create_range([], from, to + 1)
      from > to   -> create_reversed_range([], to, from + 1)
      from == to  -> [from]
      true        -> {:error, :undefined}
    end
  end

  defp create_range(list, a, a), do: list
  defp create_range(list, a, b), do: create_range(list ++ [a], a + 1, b)

  defp create_reversed_range(list, a, a), do: list
  defp create_reversed_range(list, a, b), do: create_reversed_range([a | list], a + 1, b)

end
