defmodule FactorialServer do
  def start do
    spawn(__MODULE__, :loop, [self()])
  end
  def loop(from) do
    receive do
      {:run, value} when is_number(value) and value >= 0 ->
        send(from, {:ok, Factorial.fact(value)})
        loop(from)

      {:run, _} ->
        send(from, {:error, :bad_arg})
        loop(from)

      {:bye} ->
        :ok

      _ ->
        send(from, {:error, :illegal_arg})
        loop(from)
    end
  end
end
